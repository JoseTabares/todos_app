import 'package:flutter/material.dart';
import 'package:todos_app/home_bloc.dart';
import 'package:todos_app/manage_todo_page.dart';
import 'package:todos_app/my_database.dart';

import 'todo.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomeBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = HomeBloc();
    MyDatabase().init().then((value) {
      bloc.getTodos();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Todos'),
      ),
      body: StreamBuilder<List<Todo>>(
        initialData: [],
        stream: bloc.todos,
        builder: (context, snapshot) {
          return ListView.builder(
            itemBuilder: (context, index) => Card(
              child: ListTile(
                title: Text(snapshot.data[index].title),
                subtitle: Text(snapshot.data[index].subtitle),
              ),
            ),
            itemCount: snapshot.data.length,
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(
            builder: (context) => ManageTodoPage(),
          ))
              .then((value) {
            if (value ?? false) {
              bloc.getTodos();
            }
          });
        },
      ),
    );
  }
}
