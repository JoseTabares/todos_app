import 'package:rxdart/rxdart.dart';
import 'package:todos_app/my_database.dart';
import 'package:todos_app/repository.dart';
import 'package:todos_app/todo.dart';

import 'bloc.dart';

class HomeBloc with Bloc {
  final _todosSubject = BehaviorSubject<List<Todo>>();

  ValueStream<List<Todo>> get todos => _todosSubject.stream;

  @override
  void dispose() {
    _todosSubject.close();
  }

  void getTodos() {
    MyDatabase().getTodos().then((value) {
      _todosSubject.value = value;
    });
    Repository().getTodos().then((value) {
      MyDatabase().saveTodos(value);
      _todosSubject.value = value;
    });
  }
}
