import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:todos_app/todo.dart';

class MyDatabase {
  static final MyDatabase _singleton = MyDatabase._internal();

  Database database;

  factory MyDatabase() {
    return _singleton;
  }

  MyDatabase._internal();

  Future<void> init() async {
    var dir = await getApplicationDocumentsDirectory();
    await dir.create(recursive: true);

    var dbPath = '${dir.path}/my_database.db';
    this.database = await databaseFactoryIo.openDatabase(dbPath);
  }

  Future<List<Todo>> getTodos() async {
    var store = stringMapStoreFactory.store('todos');
    var records = await store.find(database);
    return records.map((e) => Todo.fromJson(e.value)).toList();
  }

  Future<void> saveTodos(List<Todo> values) async {
    var store = stringMapStoreFactory.store('todos');
    await database.transaction((transaction) async {
      await store.delete(transaction);

      for (var value in values) {
        await store.record(value.id).put(transaction, value.toJson());
      }
    });
  }
}
