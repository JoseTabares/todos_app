import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:todos_app/todo.dart';

class Repository {
  static final Repository _singleton = Repository._internal();

  List<Todo> todos;

  factory Repository() {
    return _singleton;
  }

  Repository._internal() {
    todos = [];
  }

  Future<bool> crear(Todo todo) async {
    final response = await http.post(
      'https://5da08e004d823c0014dd3bf8.mockapi.io/api/todos',
      body: jsonEncode(todo.toJson()),
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception('Failed to load album');
    }
  }

  Future<List<Todo>> getTodos() async {
    final response =
        await http.get('https://5da08e004d823c0014dd3bf8.mockapi.io/api/todos');
    if (response.statusCode == 200) {
      var list = jsonDecode(response.body) as List;
      return list.map((e) => Todo.fromJson(e)).toList();
    } else {
      throw Exception('Failed to load album');
    }
  }
}
