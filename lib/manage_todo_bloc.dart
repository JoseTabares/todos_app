import 'package:rxdart/rxdart.dart';
import 'package:todos_app/repository.dart';
import 'package:todos_app/todo.dart';

import 'bloc.dart';

class ManageTodoBloc with Bloc {
  final _titleErrorSubject = BehaviorSubject<String>();
  final _subtitleErrorSubject = BehaviorSubject<String>();

  ValueStream<String> get titleError => _titleErrorSubject.stream;
  ValueStream<String> get subtitleError => _subtitleErrorSubject.stream;

  @override
  void dispose() {
    _titleErrorSubject.close();
    _subtitleErrorSubject.close();
  }

  Future<bool> create(Todo todo) async {
    bool hasError = false;
    if (todo.title.isEmpty) {
      _titleErrorSubject.value = 'Este campo es requerido';
      hasError = true;
    }

    if (todo.subtitle.isEmpty) {
      _subtitleErrorSubject.value = 'Este campo es requerido';
      hasError = true;
    }

    if (!hasError) {
      return await Repository().crear(todo);
    }

    return false;
  }
}
