import 'package:flutter/material.dart';
import 'package:todos_app/manage_todo_bloc.dart';

import 'todo.dart';

class ManageTodoPage extends StatefulWidget {
  @override
  _ManageTodoPageState createState() => _ManageTodoPageState();
}

class _ManageTodoPageState extends State<ManageTodoPage> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _subtitleController = TextEditingController();

  String titleError;
  String subtitleError;
  ManageTodoBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = ManageTodoBloc();
  }

  @override
  void dispose() {
    _titleController.dispose();
    _subtitleController.dispose();
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Crear'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              StreamBuilder<String>(
                stream: bloc.titleError,
                builder: (context, snapshot) {
                  return TextField(
                    controller: _titleController,
                    decoration: InputDecoration(
                      labelText: 'Titulo',
                      hintText: 'Cualquier cosa',
                      errorText: snapshot.data,
                    ),
                  );
                },
              ),
              StreamBuilder<String>(
                stream: bloc.subtitleError,
                builder: (context, snapshot) {
                  return TextField(
                    controller: _subtitleController,
                    decoration: InputDecoration(
                      labelText: 'Subtitulo',
                      hintText: 'Cualquier cosa',
                      errorText: snapshot.data,
                    ),
                  );
                },
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: RaisedButton(
                  child: Text('Crear'),
                  onPressed: manageTodoOnPressed,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> manageTodoOnPressed() async {
    var title = _titleController.text.trim();
    var subtitle = _subtitleController.text.trim();

    var todo = Todo(title: title, subtitle: subtitle);

    var result = await bloc.create(todo);
    if (result) {
      Navigator.of(context).pop(true);
    }
  }
}
